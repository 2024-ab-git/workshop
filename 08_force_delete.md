## 08 May the force be with you
No ok, w poprzednim ćwiczeniu [SPOILER ALERT] wysłaliśmy nasz klucz (`credentials/keys.txt`) na serwer zdalny i łatwo można go odczytać cofając się w historii commitów. Jak zapewne pamiętasz git równie dobrze radzi sobie z trzymaniem historii w środowisku lokalnym więc tutaj również łatwo możemy cofnąć się do naszego sekretnego klucza. Sprawdźmy.

1. Wykonaj polecenie `git reset HEAD~1 --hard`
2. Sprawdź czy Twój klucz jest widoczny w systemie plików.

Teraz plan jest taki, żeby klucz na dobre wywalić z serwera zdalnego jednocześnie zatrzymując go na lokalnym komputerze.

1. Commit w którym obecnie znajduje się nasz `HEAD` czyli my, to właśnie ten, w którym dodaliśmy klucz, którego nie chcieliśmy dodać do repozytorium. Cofnijmy się zatem jescze jeden commit, do momentu w którym tego klucza nie dodaliśmy do indeksu, tym razem poleceniem `git reset HEAD~1 --mixed`.
2. Sprawdź `git status` i upewnij się, że klucz dalej znajduje się na Twoim komputerze, jednocześnie nie znajdując się w indeksie.
3. Jeżeli tak jest to zmodyfikuj plik `.gitignore`, tak żeby na przyszłość ignorował również plik `keys.txt`
3. Wygląda na to, że wszystko powinno być ok, klucz pozostał na Twoim komputerze jednocześnie nie jest już więcej wersjonowany. 

    Niestety, podczas tych warsztatów dwukrotnie wykonaliśmy polecenie `reset HEAD~1`, czyli efektywnie cofnęliśmy się o 2 commity. Sprawdź poleceniem `git status` czy to prawda. Powinna być wiadomość "...your branch is 2 commits behind...". Oznacza to tyle, że na serwerze zdalnym te commity są dostępne.
3. Aby zmienić ten stan rzeczy musimy je usunąć ze zdalnego brancha. Możesz to zrobić poleceniem `git push -f`

> **Uwaga! Achtung!**
>
> Flaga `-f`, czy też `--force` to z reguły super niebiezpieczna zabawa. Unikaj jej jak ognia. W naszym setupie konieczna będzie zmiana ustawień twojego repozytorium tak, żeby pozwolić nawet na jej wykonanie.