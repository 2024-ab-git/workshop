## 11 Stash - chomikujemy na później
Teraz zasymulujemy sytuację w której pracujesz nad jakąś rzeczą (plikem `allowlist.txt`), ale musisz przełączyć kontekst i naprawić/zmienić coś innego (zmienić plik `README.md`).

1. W celu wycięcia ruchu z facebooka chcesz w `allowlist.txt` wpisać linię `!facebook.com*`. Nasz plik będzie teraz wyglądał tak:
    ```
    localhost
    *
    !facebook.com*
    ```
2. Nie masz jednak pewności, że to odpowiedni sposób zapisu tej reguły, musisz to z kimś zweryfikować, a jednocześnie ktoś pilnie prosi Cię o zmianę nagłówka w pliku `README.md` na `# Super Git Project`.
3. Odłóż obecne w twoim working directory zmiany na bok poleceniem `git stash`. Sprawdź, czy wpis z facebookiem zniknął z pliku `allowlist.txt`.
4. Zmień nagłówek w pliku `README.md` na `# Super Git Project`.
5. Wyślij zmiany na serwer.
6. Teraz, żeby wrócić do swojej pracy użyj polecenia `git stash pop`
7. Możesz na spokojnie kombinować, czy składnia `!facebook.com*` jest poprawna.