## 09 Revert - godne cofanie zmian na zdalnym serwerze
W poprzednich warsztatach cofaliśmy zmiany na serwerze zdalnym w sposób niebezpieczny używając flagi `--force`. Podkreślę, że jest to rozwiązanie ostateczne, które powinno być możliwe na branchach publicznych (`main`,`dev`, `qa`, etc.) tylko w wyjątkowych przypadkach, np. ze względu na wymogi prawne. W kwestii haseł **zawsze** warto zmienić takie hasło po wpadce i wtedy obecność w historii starego nie ma już takiego znaczenia.

No ale do brzegu. Zobaczmy jak cofać zmiany w sposób bardziej zgodny z dobrymi praktykami. Dalej pracujemy w naszym repo `01-local-repo`.

1. Do pliku `allowlist.txt` dodaj linię z wildcard'em,  powinien on wyglądać tak:
    ```
    localhost
    *
    ```
    Wcześniej posiadał on tylko wpis localhost.
2. Pushujemy na serwer (`add`, `commit`, `push`).
3. Okazuje się, że wpuszczanie każdego hosta do naszej aplikacji, nie jest dobrym pomysłem, dlatego chcemy cofnąć tę zmianę. 

    Gdybyśmy nie wykonali push'a moglibyśmy to cofnąć poprzez... (no właśnie to zadanie dodatkowe, podziel się pomysłem z prowadzącym). 

    W każdym razie pusha wykonaliśmy więc posprzatamy to w godny sposób.

4. Wpisz git log i znajdź hash commita, gdzie zmieniliśmy niekorzystnie `allowlist.txt`. Zazwyczaj wystarczy tylko pierwsze 6 znaków hasha, ale możesz skopiować cały.
5. Wykonaj polecenie `git revert <ten_hash>`
6. Pojawi się prośba o ustawienie nazwy commita cofającego, można to obejść dodając flagę `--no-edit`, czyli `git revert <ten_hash> --no-edit`
7. Sprawdź stan drzewa poleceniem `git log` następnie wykonaj pusha.