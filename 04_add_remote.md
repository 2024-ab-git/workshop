## 04 Stwórz własne zdalne repozytorium
1. Stwórz nowy projekt używając tego [linka](https://gitlab.com/projects/new)
![alt text](img/03_01.png)
2. Stwórz projekt
![alt text](img/03_02.png)
3. Teraz wejdź w terminalu do projektu, który stworzyliśmy w warsztacie pierwszym
    ```sh
    cd ~/workspace/git-workshops/01-local-repo
    ```
4. Poniższe polecenia wyślą Twoje lokalne repozytorium do zdalnego serwera
    ```sh
    git remote add origin git@gitlab.com:01-local-repo-<twoj_nick>.git
    git push --set-upstream origin --all
    git push --set-upstream origin --tags
    ```
3. Wejdź w przeglądarce do stworzonego w tym warsztacie projektu GitLab. Sprawdź czy widzisz plik `README.md` oraz czy jest on identyczny z jego wersją na lokalnym komputerze.

4. Wykorzystaj dotychczasową wiedzę tak aby oba pliki (lokalny i zdalny) były identyczne