## 01 Health check
1. Stwórz terminal (np. Git Bash) i wpisz `git` i upewnij się, że komenda jest rozpoznawalna.
2. Skonfiguruj swoją wizytówkę
    ```sh
    git config --global user.name "Imie Nazwisko"
    git config --global user.email "twoj@email.com"
    ```
3. Przenieś się do folderu domowego `cd`
4. Stwórz folder na zajecia `mkdir –p workspace/git-workshops`
5. Wejdź do środka `cd workspace/git-workshops`
6. Stwórz pierwsze repozytorium
    ```sh
    mkdir 01-local-repo
    cd 01-local-repo
    git init
    ```
7. Z poziomu eksploratora plików lub linii komend (`ls -la`) zweryfikuj czy pojawił się folder `.git` 