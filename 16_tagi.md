## 16 Tagowanie
1. **Przygotowanie repozytorium**:
   - Stwórz nowe lokalne repozytorium (`16-tagowanie`): `git init`
   - Zainicjuj repozytorium z plikiem README: `echo "# Mój Projekt" > README.md` oraz `git add README.md` i `git commit -m "Początkowy commit"`

2. **Stwórz zdalne repozytorium**:
   - Przejdź do usługi hostingowej Git (GitLab) i stwórz nowe zdalne repozytorium.
   - Skopiuj adres SSH zdalnego repozytorium.
   - Dodaj zdalne repozytorium do swojego lokalnego repozytorium: `git remote add origin <adres-zdalnego-repo>`

3. **Stwórz gałąź dla wydania**:
   - Stwórz nową gałąź o nazwie `feature-branch`: `git checkout -b feature-branch`
   - Stwórz nowy plik `feature.txt`: `touch feature.txt` i dodaj treść
   - Dodaj i commitnij zmiany: `git add feature.txt` oraz `git commit -m "Implementacja nowej funkcjonalności"`

4. **Scal feature brancha**
    - Przełącz się na brancha głównego i scal feature brancha

4. **Stwórz tag dla wydania**:
   - Będąc na gałęzi `master`, stwórz nowy tag: `git tag v1.0`
   - Opcjonalnie możesz dodać wiadomość lub adnotację do taga: `git tag -a v1.0 -m "Wydanie wersji 1.0"`

5. **Wypchnij gałąź wydaniową i tag do zdalnego repozytorium**:
   - Wypchnij gałąź `master` do zdalnego repozytorium: `git push -u origin master`
   - Wypchnij tag `v1.0` do zdalnego repozytorium: `git push origin v1.0`

**Cheatsheet**
```sh
# creation
git tag -a v1.4 -m "my version 1.4" #annotated tag
git tag v1.4 #lightweight tag
git tag -a v1.2 9fceb02 # tagging historical commit

# view
git tag #all tags, alphabetically
git show v1.4 #single tag

# sharing
git push origin v1.5 # sharing one
git push origin --tags # sharing all

# deleting
git tag -d v1.4-lw #delete local tag
git push origin --delete <tagname> # push deletion to remote

# switching to tags
git checkout v2.0.0 # just look around
git switch -c v2.0.1-security-fix # if you want to start tracable work
git tag -a v2.0.1-security-fix -m "urgent security thread fix for older clients"
```