## 07 RM deep dive
Kontynuujemy pracę w `01-local-repo`.

1. W folderze `credentials` stwórz plik `keys.txt` o zawartości
    ```
    my secret key
    ```
2. Wykonaj push na serwer (add, commit, push)
3. Właśnie wysłałeś/aś wrażliwy plik na serwer, niedobrze. Usuńmy go stamtąd. W terminalu wykonaj polecenie `git rm credentials/keys.txt`
4. Wykonaj polecenie `git status` i zaobserwuj co tam się wydarzyło.
5. Zatwierdź zmiany (`commit`) i wyślij na serwer (`push`).
5. Sprawdź, czy klucze zniknęły z serwera 
6. Zakładając, że wykonałaś/eś poprzednie polecenia z prędkością światła, czy Twoje klucze są bezpieczne?