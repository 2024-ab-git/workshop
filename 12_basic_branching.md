## 12 Basic branching
1. **Przygotowanie repozytorium**:
   - Stwórz nowy katalog dla projektu: `mkdir 12-basic-branching`
   - Przejdź do katalogu projektu: `cd 12-basic-branching`
   - Zainicjuj nowe repozytorium Git: `git init`
   - Stwórz plik README: `touch README.md`
   - Dodaj plik README do obszaru przejściowego: `git add README.md`
   - Commitnij początkowe zmiany: `git commit -m "Początkowy commit"`

2. **Stwórz nową gałąź**:
   - Sprawdź bieżącą gałąź: `git branch` (powinna pokazać `* master`)
   - Stwórz nową gałąź o nazwie "feature-branch": `git branch feature-branch`
   - Przełącz się na nową gałąź: `git checkout feature-branch`
   - Opcjonalnie `git checkout -b feature-branch` lub `git switch -c feature-branch`

3. **Wprowadź zmiany na nowej gałęzi**:
   - Stwórz nowy plik "feature.txt": `touch feature.txt`
   - Dodaj treść do pliku "feature.txt" za pomocą edytora tekstu
   - Dodaj nowy plik do obszaru przejściowego: `git add feature.txt`
   - Commitnij zmiany: `git commit -m "Implementacja nowej funkcjonalności"`

4. **Przełącz się z powrotem na główną gałąź**:
   - Przełącz się z powrotem na główną gałąź (master): `git checkout master`
   - Zauważ, że plik "feature.txt" nie jest obecny na głównej gałęzi

5. **Stwórz kolejną gałąź i scal ją**:
   - Stwórz nową gałąź o nazwie "bugfix-branch": `git branch bugfix-branch`
   - Przełącz się na gałąź "bugfix-branch": `git checkout bugfix-branch`
   - Zmodyfikuj plik README, aby naprawić błąd lub dodać dokumentację
   - Dodaj zmiany do obszaru przejściowego: `git add README.md`
   - Commitnij zmiany: `git commit -m "Naprawienie błędu w README"`
   - Przełącz się z powrotem na główną gałąź: `git checkout master`
   - Scal gałąź "bugfix-branch" do głównej gałęzi: `git merge bugfix-branch`

6. **Scal gałąź z funkcjonalnością**:
   - Przełącz się z powrotem na gałąź "feature-branch": `git checkout master`
   - Scal gałąź "feature-branch" do głównej gałęzi: `git merge feature-branch`
   - Rozwiąż wszelkie konflikty, które mogą wystąpić podczas scalania

7. **Posprzątaj gałęzie**:
   - Wyświetl wszystkie gałęzie: `git branch`
   - Usuń gałąź "feature-branch": `git branch -d feature-branch`
   - Usuń gałąź "bugfix-branch": `git branch -d bugfix-branch`