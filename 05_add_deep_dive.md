## 05 Add deep dive
1. Otwórz w edytorze projekt `01-local-repo`
4. Lokalnie dodaj do projektu pliki tak, żeby tworzyły następującą strukturę
    ```sh
    .gitignore
    README.md
    allowlist.txt
    credentials\
        |
        |passwords.txt
    images\
    ```
4. Wpisz polecenie `git status` i zaobserwuj, czy widoczny jest tam plik `passwords.txt`? Naturalnie nie jest to plik, który chcemy wysyłać do repozytorium. 
4. Otwórz plik `.gitignore` i wpisz tam następującą linię
    ```
    credentials/passwords.txt
    ```
4. Wpisz polecenie `git status` i zaobserwuj, czy widoczny jest tam plik `passwords.txt`? Powinien zniknąć.
4. Dodaj wszystkie pliki do staging area poleceniem `git add -A`
6. Zrób commita i push'nij go na serwer
7. Sprawdź jakie pliki widoczne są na zdalnym serwerze. Zauważ, które pliki są widoczne w twoim lokalnym systemie plików, a których nie widać na serwerze.
8. Wróć do lokalnego repozytorium i stwórz w folderze plik `images` pusty plik `.gitkeep`
9. Dodaj zmiany do staging area, wykonaj `commit` i `push`
10. Sprawdź czy teraz plik jest widoczny.