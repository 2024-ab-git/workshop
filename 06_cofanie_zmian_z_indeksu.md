## 06 Usuwanie zmian z indeksu/staging area
Kontynuujemy pracę w `01-local-repo`.
1. Wejdź do pliku `README.md` i dodaj na końcu linię tekstu
    ```
    ...
    This is a terrible mistake.
    ```
2. Z kolei w pliku `allowlist.txt` dodaj linię
    ```
    localhost
    ```
2. Następnie dodaj te zmiany do indeksu/staging area (`git add`)
3. Jak się możesz zorientować dodanie zmiany do `README.md` było straszną pomyłką. Nie chcesz tego pushować. Cofnij to poleceniem `git restore --staged README.md`
4. Sprawdź czy pechowa linia dalej występuje w pliku? Zastanów się co się wydarzy jeżeli wykonasz teraz commit?
5. Wykonaj teraz polecenie `git restore README.md`. Jaki jest rezultat?