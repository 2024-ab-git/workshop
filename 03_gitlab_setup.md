## 03 GitLab setup
1. W terminalu wykonaj poniższe polecenie, jeżeli nie ma żadnego rezultatu przejdź do kroku **4**
    ```sh
    cat ~/.ssh/id_ed25519.pub
    ```
2. Otwórz terminal i wykonaj poniższe polecenie. Jeżeli pojawią się jakieś pytania naciskaj ENTER
    ```sh
    ssh-keygen -t ed25519
    ```
2. Następnie wyświetl wygenerowany klucz publiczny poleceniem
    ```sh
    cat ~/.ssh/id_ed25519.pub
    ```
1. Zarejestruj się na stronie [GitLab](https://gitlab.com/users/sign_up)
2. Podziel się swoim emailem lub @loginem z prowadzącym
![github handle](img/02_01_handle.png)
3. Przejdź do ustawień 
![alt text](img/02_02_preferences.png)
4. Następnie do sekcji generowania kluczy SSH
![alt text](img/02_03_ssh_keys.png)
5. Kliknij "Add new key"
![alt text](img/02_04_add_ssh_key.png)
6. Wklej wygenerowany klucz do nowo otwartego okna i zapisz przyciskiem "Add key"
7. Wróć do terminala i wykonaj następujące polecenia
```sh
cd ~/workspace/git-workshops/
git clone git@gitlab.com:2024-ab-git/02_gitlab_healthcheck.git
```
8. Zobacz, co sciągnęło się na Twój lokalny komputer