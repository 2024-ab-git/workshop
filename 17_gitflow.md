## 17 Git flow
1. **Przygotowanie repozytorium**:
   - Stwórz nowe repozytorium: `git init`
   - Zainicjuj repozytorium z plikiem README: `echo "# Mój Projekt" > README.md` oraz `git add README.md` i `git commit -m "Początkowy commit"`
   - Stwórz następujące gałęzie:
     - `dev`: `git branch dev`

2. **Stwórz gałęzie funkcjonalne**:
   - Przełącz się na gałąź `dev`: `git checkout dev`
   - Stwórz gałąź `feature-a`: `git checkout -b feature-a`
   - Stwórz nowy plik `feature-a.txt`: `touch feature-a.txt` i dodaj treść
   - Zmień nieznacznie nagłówek w `README.md`
   - Dodaj i commitnij zmiany: `git add .` oraz `git commit -m "Implementacja funkcjonalności A"`
   - Przełącz się na gałąź `dev`: `git checkout dev`
   - Stwórz gałąź `feature-b`: `git checkout -b feature-b`
   - Stwórz nowy plik `feature-b.txt`: `touch feature-b.txt` i dodaj treść
   - Zmień nieznacznie nagłówek w `README.md`
   - Dodaj i commitnij zmiany: `git add .` oraz `git commit -m "Implementacja funkcjonalności B"`

3. **Wprowadź konflikty**:
   - Przełącz się na gałąź `dev`: `git checkout dev`
   - Zmodyfikuj plik README (np. dodaj nową linię lub zmień tekst): `nano README.md`
   - Commitnij zmiany: `git add README.md` oraz `git commit -m "Aktualizacja README"`

4. **Scal gałęzie funkcjonalne do gałęzi deweloperskiej**:
   - Scal gałąź `feature-a` do gałęzi `dev`: `git merge feature-a`
   - Rozwiąż wszelkie konflikty, które mogą wystąpić podczas scalania (np. konflikty w pliku README)
   - Commitnij rozwiązane konflikty: `git add .` oraz `git commit -m "Rozwiązanie konfliktów"`
   - Scal gałąź `feature-b` do gałęzi `dev`: `git merge feature-b`
   - Rozwiąż wszelkie konflikty, które mogą wystąpić podczas scalania
   - Commitnij rozwiązane konflikty: `git add .` oraz `git commit -m "Rozwiązanie konfliktów"`

5. **Stwórz gałąź wydaniową**:
   - Z gałęzi `dev` stwórz nową gałąź `release-v1.0`: `git checkout -b release-v1.0`
   - Wprowadź zmianę w pliku `README.md` wpisując na dole to jest wersja v1.0
   - Zacommituj zmiany

6. **Scal gałąź wydaniową do gałęzi produkcyjnej**:
   - Przełącz się na gałąź `master`: `git checkout master`
   - Scal gałąź `release-v1.0` do gałęzi `master`: `git merge release-v1.0`
   - Rozwiąż wszelkie konflikty, które mogą wystąpić podczas scalania
   - Commitnij rozwiązane konflikty: `git add .` oraz `git commit -m "Rozwiązanie konfliktów"`
   - Stwórz tag dla wydania produkcyjnego: `git tag v1.0`

7. **Zaktualizuj gałąź deweloperską**:
   - Przełącz się na gałąź `dev`: `git checkout dev`
   - Scal gałąź `release-v1.0` do gałęzi `dev`: `git merge release-v1.0`
   - Rozwiąż wszelkie konflikty, które mogą wystąpić podczas scalania
   - Commitnij rozwiązane konflikty: `git add .` oraz `git commit -m "Rozwiązanie konfliktów"`

8. **Posprzątaj gałęzie**:
   - Usuń gałęzie `feature-a` i `feature-b`: `git branch -d feature-a feature-b`
   - Opcjonalnie możesz również usunąć gałąź `release-v1.0`: `git branch -d release-v1.0`