## 10 Cherry Pick - wisienki z tortu
No ok, sytuacja się zmieniła, okazuje się, że dodanie `*` do pliku `allowlist.txt` było jednak dobrym pomysłem. Chcemy to zrobić ponownie. Jakie mamy możliwości?
1. Zrobić reverta ostatniego commita, czyli efektywnie "reverta reverta" (nie mylić z "sera sera").
2. Po prostu dodać `*` do pliku i zrobić nowego commita.
3. Albo...

No właśnie tutaj można użyć również polecenia `git cherry-pick`. Polecenie to aplikuje zmiany zaaplikowane w konkretnym commicie.

1. Przy użyciu polecenia `git log` odnajdź hash commita, który w niedawnej historii dodawał nam `*` do pliku `allowlist.txt`
2. Następnie wykonaj polecenie `git cherry-pick <ten_hash>`
3. I teraz `git push`.

Tak oto przywróciliśmy zmiany z poprzedniego commita. W tym wypadku była to tylko jedna gwiazdka, natomiast kiedy zmian jest więcej i to nie w jednym a wielu plikach, te polecenie może się przydać.