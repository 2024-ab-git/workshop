## 02 Pierwszy commit 
1. W wybranym przez Ciebie edytorze plików stwórz plik `README.md`
2. W środku wpisz następującą treść
    ```markdown
    # My Git project
    Hello Git
    1. Git
    1. Is 
    1. **Cool** for `code`
    ```
3. Sprawdź stan working directory poleceniem ```git status```
4. Dodaj plik `README.md` do staging area poleceniem `git add`
5. Jeszcze raz zmodyfikuj plik `README.md` dodając tekst
    ```markdown
    I shouldn't be commited
    ```
    Nie wykonuj polecenia `git add` po tej modyfikacji. `README.md` powinien się teraz znajdować zarówno w sekcji "staged for commit" jak również "not staged for commit"
5. Wykonkonaj commit poleceniem `git commit -m "initial commit"`