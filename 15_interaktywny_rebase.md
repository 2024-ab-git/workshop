## 15 Interaktywny rebase

**Krok 1: Przygotowanie repozytorium**

1. Stwórz nowy katalog na swój projekt i przejdź do niego.
2. Zainicjuj nowe repozytorium Git za pomocą `git init`.
3. Stwórz nowy plik `README.md` o następującej treści:

```markdown
# Mój Projekt

To jest przykładowy projekt na warsztat interaktywnego rebasu w Git.
```

4. Dodaj i commitnij plik `README.md`:

```
git add README.md
git commit -m "Początkowy commit"
```
5. Przejdź do usługi hostingowej Git (GitLab) i stwórz nowe zdalne repozytorium, nie używaj opcji tworzenia pliku README, niech to będzie puste repozytorium.
6. Skopiuj adres SSH zdalnego repozytorium.
7. Dodaj zdalne repozytorium do swojego lokalnego repozytorium: `git remote add origin <adres-zdalnego-repo>`

**Krok 2: Stwórz dodatkowe commity**

1. Stwórz nowy plik `app.js` o następującej treści:

```javascript
console.log("Hello, World!");
```

2. Dodaj i commitnij plik `app.js`:

```
git add app.js
git commit -m "Dodaj app.js"
```

3. Zmodyfikuj plik `app.js`, aby zawierał funkcję:

```javascript
console.log("Hello, World!");

function greet(name) {
  console.log(`Hello, ${name}!`);
}

greet("Alice");
```

4. Dodaj i commitnij zmiany:

```
git add app.js
git commit -m "Dodaj funkcję greet"
```

5. Ponownie zmodyfikuj plik `app.js`, aby zawierał kolejną funkcję:

```javascript
console.log("Hello, World!");

function greet(name) {
  console.log(`Hello, ${name}!`);
}

greet("Alice");

function farewell(name) {
  console.log(`Goodbye, ${name}!`);
}

farewell("Bob");
```

6. Dodaj i commitnij zmiany:

```
git add app.js
git commit -m "Dodaj funkcję farewell"
```

Teraz masz repozytorium z czterema commitami.

**Krok 3: Interaktywny rebase**

1. Uruchom polecenie interaktywnego rebasu:

```
git rebase -i HEAD~4
```

Spowoduje to otwarcie edytora z listą ostatnich czterech commitów, każdy poprzedzony słowem "pick".

2. W edytorze możesz wykonać różne operacje, zmieniając słowo "pick" dla każdego commitu. Oto niektóre przykłady:

- **Zmiana kolejności commitów**: Aby zmienić kolejność commitów, po prostu zmień kolejność wierszy w edytorze.
- **Squashowanie commitów**: Aby squashować wiele commitów w jeden, zmień słowo "pick" na "squash" (lub "s") dla commitów, które chcesz squashować, z wyjątkiem pierwszego.
- **Edycja wiadomości commitów**: Aby edytować wiadomość commitu, zmień słowo "pick" na "reword" (lub "r") dla tego commitu.
- **Usuwanie commitów**: Aby usunąć commit, zmień słowo "pick" na "drop" (lub "d") dla tego commitu.

Na przykład, powiedzmy, że chcesz squashować ostatnie dwa commity ("Dodaj funkcję greet" i "Dodaj funkcję farewell") w jeden commit i zmienić wiadomość commitu dla "Dodaj app.js". Twój edytor powinien wyglądać tak:

```
pick 1a2b3c4 Początkowy commit
reword 5e6f7g8 Dodaj app.js
squash 9h0i1j2 Dodaj funkcję greet
squash 3k4l5m6 Dodaj funkcję farewell
```

3. Zapisz i wyjdź z edytora.

4. Jeśli wybrałeś squashowanie commitów, Git otworzy kolejny edytor, w którym możesz podać nową wiadomość commitu dla squashowanego commitu.

5. Jeśli wybrałeś zmianę wiadomości commitu, Git otworzy edytor, w którym możesz zmodyfikować wiadomość commitu.

6. Po wprowadzeniu pożądanych zmian, Git wykona rebase commitów zgodnie z Twoimi instrukcjami.

**Krok 4: Eksploruj dalej**

1. Wypróbuj różne kombinacje operacji podczas interaktywnego rebasu, takie jak zmiana kolejności, squashowanie, edycja i usuwanie commitów.
2. Eksperymentuj z rebasowaniem większej liczby commitów, aby przećwiczyć zarządzanie złożonymi historiami commitów.