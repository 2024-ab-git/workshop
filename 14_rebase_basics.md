## 14 Rebase basics
**Przygotowanie repozytorium**

1. Stwórz nowy katalog na swój projekt i przejdź do niego.
2. Zainicjuj nowe repozytorium Git za pomocą `git init`.
3. Stwórz nowy plik `README.md`
4. Dodaj i commitnij plik `README.md`:
```
git add README.md
git commit -m "Początkowy commit"
```

**Ćwiczenie 14.1: Podstawowe Rebase**

1. Stwórz nową gałąź o nazwie "feature-branch" z głównej gałęzi.
1. Przełącz się na gałąź "feature-branch" i stwórz nowy plik "feature.txt".
1. Dodaj treść do pliku "feature.txt" i commitnij zmiany.
1. Przełącz się z powrotem na główną gałąź i wprowadź zmiany w pliku README.
6. Commitnij zmiany w głównej gałęzi.
7. Przełącz się z powrotem na gałąź "feature-branch".
8. Wykonaj rebase gałęzi "feature-branch" na główną gałąź używając `git rebase main`.

**Ćwiczenie 14.2: Rebase z konfliktami**

1. Zacznij od repozytorium z poprzedniego ćwiczenia.
2. Stwórz nową gałąź o nazwie "conflict-branch" z głównej gałęzi.
3. Przełącz się na gałąź "conflict-branch" i zmodyfikuj plik README.
4. Commitnij zmiany w gałęzi "conflict-branch".
5. Przełącz się z powrotem na główną gałąź i zmodyfikuj te same linie w pliku README.
6. Commitnij zmiany w głównej gałęzi.
7. Przełącz się z powrotem na gałąź "conflict-branch".
8. Spróbuj wykonać rebase gałęzi "conflict-branch" na główną gałąź używając `git rebase main`.
9. Rozwiąż konflikty i kontynuuj rebase.

**Ćwiczenie 14.3: Interaktywny Rebase**

1. Zacznij od repozytorium z poprzednich ćwiczeń.
2. Stwórz nową gałąź o nazwie "interactive-branch" z głównej gałęzi.
3. Przełącz się na gałąź "interactive-branch" i stwórz trzy oddzielne pliki: "file1.txt", "file2.txt" i "file3.txt".
4. Commitnij każdy plik oddzielnie z opisowymi wiadomościami commitów.
4. Użyj `git log`, żeby zachować na później hashe commitów
5. Uruchom interaktywny rebase używając `git rebase -i HEAD~3`.
6. W edytorze interaktywnego rebase zmień kolejność commitów, squashuj niektóre commity lub edytuj wiadomości commitów.
7. Zapisz i wyjdź z edytora, a Git wykona rebase commitów zgodnie z Twoimi instrukcjami.
7. Użyj `git log` i porównaj hashe commitów.