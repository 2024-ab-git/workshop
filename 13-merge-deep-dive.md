## 13 Merge deep dive
**13.1: Przygotowanie**

1. Stwórz nowe repozytorium `13-merge-deep-dive` i dodaj plik README. (`git init` te sprawy)
2. Stwórz plik `README.md` z jakąś treścią i wykonaj commita

**13.2: Scalanie z konfliktami**

1. Zacznij od repozytorium z poprzedniego ćwiczenia.
2. Stwórz nową gałąź o nazwie "conflict-branch" z głównej gałęzi.
3. Przełącz się na gałąź "conflict-branch" i zmodyfikuj plik `README.md`.
4. Commitnij zmiany w gałęzi "conflict-branch".
5. Przełącz się z powrotem na główną gałąź i zmodyfikuj te same linie w pliku `README.md`.
6. Commitnij zmiany w głównej gałęzi.
7. Spróbuj scalić gałąź "conflict-branch" do głównej gałęzi używając `git merge conflict-branch`.
8. Rozwiąż konflikty i commitnij rozwiązane zmiany.

**13.3: Scalanie ze squashowaniem commitów**

1. Zacznij od repozytorium z poprzednich ćwiczeń.
2. Stwórz nową gałąź o nazwie "squash-branch" z głównej gałęzi.
3. Przełącz się na gałąź "squash-branch" i stwórz nowy plik "feature1.txt".
4. Commitnij zmiany.
5. Stwórz kolejny plik "feature2.txt" i commitnij zmiany.
6. Przełącz się z powrotem na główną gałąź.
7. Scal gałąź "squash-branch" do głównej gałęzi używając `git merge --squash squash-branch`.
8. Commitnij squashowane zmiany z nową wiadomością commitu.

**13.4: Scalanie bez Fast-Forward**

1. Zacznij od repozytorium z poprzednich ćwiczeń.
2. Stwórz nową gałąź o nazwie "no-ff-branch" z głównej gałęzi.
3. Przełącz się na gałąź "no-ff-branch" i stwórz nowy plik "feature.txt".
4. Commitnij zmiany.
5. Przełącz się z powrotem na główną gałąź.
7. Scal gałąź "no-ff-branch" do głównej gałęzi używając `git merge --no-ff no-ff-branch`.